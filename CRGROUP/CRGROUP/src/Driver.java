
public class Driver {
	public static void main (String[]args){
		P5_Katta_Khan_Acharya_View theView = new P5_Katta_Khan_Acharya_View();
		P5_Katta_Khan_Acharya_Model theModel = new P5_Katta_Khan_Acharya_Model();
		theModel.initializeMap();
		P5_Katta_Khan_Acharya_Controller theController = new P5_Katta_Khan_Acharya_Controller(theView, theModel);
		theController.playGame();
	}
}
