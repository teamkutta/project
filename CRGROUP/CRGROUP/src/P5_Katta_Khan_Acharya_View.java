import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.Timer;

public class P5_Katta_Khan_Acharya_View {
	MyDrawingPanel drawingPanel;
	int numM = 0;
	Timer timer;
	Timer timer2;
	JMenuItem how;
	JMenuItem about;
	JMenuItem exit;
	JMenuItem setNum;
	JMenuItem newGame;
	JLabel stepsTaken;
	JLabel alive;
	JLabel highScore;
	Character[][] arr = new Character[10][10];
	JFrame window = new JFrame("Crossy Road with Michael Jackson's Chicken");
	P5_Katta_Khan_Acharya_View(){
		//JFrame window = new JFrame("Crossy Road with Michael Jackson's Chicken");
		window.setBounds(100, 100, 445, 600);
		window.setResizable(false);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		// create a JMenuBar
		JMenuBar menuBar = new JMenuBar();
		
		// create a JMenu called "File"
		JMenu fileMenu = new JMenu("File");
		
		// Create a "Save" JMenuItem
		newGame = new JMenuItem("New Game");
		
		// add an action listener to save item that opens a save dialog using JFileChooser
		// and prints the selected File object
		newGame.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				//theController.newGame();
			}
		});
		
		
		// add the save item to the file menu
		fileMenu.add(newGame);
		
		// create a JMenu item "Open"
		exit = new JMenuItem("Exit");
		
		// add an action listener to the open menu item that opens an open dialog with JFileChooser
		// and prints the File object selected
		exit.addActionListener(e -> {
			System.exit(0);
		});
		
		// add the open item to the file menu
		fileMenu.add(exit);
		
		// add the file menu to the menu bar
		menuBar.add(fileMenu);
		
		// add another JMenu to the menu bar
		JMenu help = new JMenu("Help");
		how = new JMenuItem("How To Play");
		about = new JMenuItem("About");
		
		// add an action listener to the open menu item that opens an open dialog with JFileChooser
		// and prints the File object selected
		how.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		
		about.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		
		help.add(how);
		help.add(about);
		menuBar.add(help);
		
		// add the menu bar to the window in the top section of the window
		window.add(menuBar, BorderLayout.NORTH);
		
		//int numM = 0;
		stepsTaken = new JLabel(String.valueOf(numM));
		stepsTaken.setBorder(BorderFactory.createTitledBorder("Score"));
		stepsTaken.setBounds(50, 450, 100, 50);
		
		highScore = new JLabel(String.valueOf(numM));
		highScore.setBorder(BorderFactory.createTitledBorder("High Score"));
		highScore.setBounds(150, 450, 100, 50);
		
		alive = new JLabel("Alive");
		//alive.setBorder(BorderFactory.createTitledBorder("High Score"));
		alive.setBounds(300, 450, 100, 50);
		
		drawingPanel = new MyDrawingPanel();
		drawingPanel.setBounds(20, 20, 400, 400);
		drawingPanel.setBorder(BorderFactory.createEtchedBorder());
		
		for(int p = 0; p < 10; p++){
			for(int o = 0; o < 10; o++){
				arr[p][o] = '-';
				drawingPanel.arr[p][o] = '-';
			}
		}
		drawingPanel.initialize();
		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(null);
		mainPanel.setPreferredSize(new Dimension(400, 550));
		mainPanel.add(drawingPanel);
		mainPanel.add(stepsTaken);
		mainPanel.add(highScore);
		mainPanel.add(alive);
		
		
		
		window.getContentPane().add(mainPanel);

		// Let there be light
		window.setVisible(true);
		
	}
	
	public void actionPerformed(ActionEvent e) {

		/*if (e.getSource().equals(timer)) {
			text1.setText("Time: " + ++time);
		}*/
		if (e.getActionCommand() != null) {
			if (e.getSource() == exit) {
				//sequencer.stop();
				System.exit(0);
			}
			if (e.getSource() == newGame) {
				//initializeBoard();
			}
			if (e.getSource() == about) {
				
			}
			if (e.getSource() == how) {
				
			}
			

		}
	}

}


class MyDrawingPanel extends JPanel {
	Character[][] arr = new Character[10][10];
	static final char EMPTY = 'o';
	static final char TREE = 't';
	static final char STREET = 's';
	static final char CAR1 = '1';
	static final char CAR2 = '2';
	static final char CHICKEN = 'p';
	BufferedImage tree;
	BufferedImage ground;
	BufferedImage car1;
	BufferedImage road;
	BufferedImage chicken;
	BufferedImage car2;
	
	public void initialize(){
		try {
			tree = ImageIO.read(new File("tree.png"));
			ground = ImageIO.read(new File("ground.png"));
			car1 = ImageIO.read(new File("car1.png"));
			road = ImageIO.read(new File("road.gif"));
			chicken = ImageIO.read(new File("chicken.gif"));
			car2 = ImageIO.read(new File("car2.png"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public void paintComponent(Graphics g) {
		
		g.setColor(Color.lightGray);
		for (int x = 0; x < this.getWidth(); x += 40)
			g.drawLine(x, 0, x, this.getHeight());

		for (int y = 0; y < this.getHeight(); y += 40)
			g.drawLine(0, y, this.getWidth(), y);
		
		//creates random array to be drawn
		//this double for loop must be removed and the arr in the drawingpanel class must be set equal to the one in the model through the controller
		/*for (int p = 0; p < 10; p++){
			for(int o = 0; o < 10; o++){
				int random = (int )(Math. random() * 5 + 1);
				if(random == 1){
					arr[p][o] = TREE;
				}else if(random == 2){
					arr[p][o] = EMPTY;
				}else if(random == 3){
					arr[p][o] = CAR;
				}else if(random == 4){
					arr[p][o] = STREET;
				}else if(random == 5){
					arr[p][o] = CHICKEN;
				}
			}
		}*/
		/*try {
			tree = ImageIO.read(new File("tree.png"));
			ground = ImageIO.read(new File("ground.png"));
			car1 = ImageIO.read(new File("car1.png"));
			road = ImageIO.read(new File("road.gif"));
			chicken = ImageIO.read(new File("chicken.gif"));
			car2 = ImageIO.read(new File("car2.png"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		
		for(int i = 0; i < 10; i++){
			for(int j = 0; j < 10; j++){
				 BufferedImage img;
				if(arr[j][i] == TREE){
					img = tree;
					g.drawImage(img, i * 40, j * 40, 40, 40, null);
				}else if(arr[j][i] == EMPTY){
					img = ground;
					g.drawImage(img, i * 40, j * 40, 40, 40, null);
				}else if(arr[j][i] == CAR1){
					img = car1;
					g.drawImage(img, i * 40, j * 40, 40, 40, null);
				}else if(arr[j][i] == STREET){
					img = road;
					g.drawImage(img, i * 40, j * 40, 40, 40, null);
				}else if(arr[j][i] == CHICKEN){
					img = chicken;
					g.drawImage(img, i * 40, j * 40, 40, 40, null);
				}else if(arr[j][i] == CAR2){
					img = car2;
					g.drawImage(img, i * 40, j * 40, 40, 40, null);
				}
				/*img = ImageIO.read(new File("ariel.jpeg"));
				g.drawImage(img, i * 20, j * 20, 20, 20, null);*/
			}
		}

	}
}