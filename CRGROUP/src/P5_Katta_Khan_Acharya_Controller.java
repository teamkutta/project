import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import javax.swing.ImageIcon;
import javax.swing.Timer;

public class P5_Katta_Khan_Acharya_Controller implements KeyListener, ActionListener{
	//Character[][] arr = new Character[10][10];
	private P5_Katta_Khan_Acharya_Model theModel;
	private P5_Katta_Khan_Acharya_View theView;
	public static final char MINE = '*';
	Scanner console = new Scanner(System.in);
	long timestamp;
	
	P5_Katta_Khan_Acharya_Controller(P5_Katta_Khan_Acharya_View theView, P5_Katta_Khan_Acharya_Model theModel){
		this.theModel = theModel;
		this.theView = theView;
		
		/*initialize();
		addKey();
		addTimer();
		playGame();
		setEqual();
		repaint();*/
	}
	    private int dx;
	    private int dy;
	    private int x;
	    private int y;
	    private Image image;
	    
	    private void initialize() {    
	        ImageIcon im = new ImageIcon("chicken.gif");
	        image = im.getImage();
	        this.theView.newGame.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					newGame();
				}
			});
	        //x = 40;
	        //y = 60;        
	    }


	    /*public void move() {
	        this.theModel.chicken2.addX(dx);
	        this.theModel.chicken2.addY(dy);
	        this.theModel.moveChicken();
	    }*/

	    /*public int getX() {
	        return x;
	    }

	    public int getY() {
	        return y;
	    }*/

	    public Image getImage() {
	        return image;
	    }

	    public void keyPressed(KeyEvent e) {

	        int key = e.getKeyCode();

	        if (key == KeyEvent.VK_LEFT) {
	            dy = -1;
	            theModel.moveLeft();
	        }

	        if (key == KeyEvent.VK_RIGHT) {
	        	//System.out.println("right");
	            dy = 1;
	            theModel.moveRight();
	        }

	        if (key == KeyEvent.VK_UP) {
	            dx = -1;
	            theModel.moveUp();
	        }

	        if (key == KeyEvent.VK_DOWN) {
	            dx = 1;
	            theModel.moveDown();
	        }
	        setEqual();
			repaint();
	    }

	    public void keyReleased(KeyEvent e) {
	        
	        int key = e.getKeyCode();

	        if (key == KeyEvent.VK_LEFT) {
	            dx = 0;
	        }

	        if (key == KeyEvent.VK_RIGHT) {
	            dx = 0;
	        }

	        if (key == KeyEvent.VK_UP) {
	            dy = 0;
	            this.theModel.score += 1;
	        }

	        if (key == KeyEvent.VK_DOWN) {
	            dy = 0;
	            this.theModel.score -= 1;
	        }
	        setStepNum();
	        
	    }
	

	public void playGame(){
		initialize();
		addKey();
		addTimer();
		//playGame();
		setEqual();
		repaint();
	}
	
	public void setEqual(){
		for(int i = 0; i < 10; i++){
			for(int j = 0; j < 10; j++){
				this.theView.drawingPanel.arr[i][j] = this.theModel.map[i][j];
			}
		}
	}
	
	public void repaint(){
		this.theView.drawingPanel.repaint();
	}
	
	public void newGame(){
		this.theModel.score = 0;
		this.theModel.counter = 0;
		this.theModel.initializeMap();
		/*setEqual();
		repaint();*/
		playGame();
		
	}
	
	
	public boolean inArray(int row, int col){
		boolean ret = false;
		return ret;
	}


	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	public void addKey() {
		this.theView.window.addKeyListener(this);
	}
	public void addTimer() {
		this.theView.timer = new Timer(100, this);
		//this.theView.timer.addActionListener(this);
		this.theView.timer.start();
		timestamp = new java.util.Date().getTime();
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if((e.getWhen() - timestamp) % 40 == 0){
			this.theModel.initializeTopLine();
			setEqual();
			repaint();
		}
		if((e.getWhen() - timestamp) % 10 == 0){
			this.theModel.act();
			setEqual();
			repaint();
			if(theModel.isDead()){
				theView.timer.stop();
				this.theView.window.removeKeyListener(this);
				System.out.println("You Lost. =(");
				System.out.println("Score = " + theModel.score);
				/*System.out.println("Do you want to play again? y/n");
				String choice = new String("");
				choice = console.next();
				if(choice.equals("y")){
					newGame();
				}*/
			}
		}
		//this.theModel.act();
		
	}
	public void setStepNum(){
		this.theView.stepsTaken.setText(String.valueOf(theModel.score));
	}
	
}
