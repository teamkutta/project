import java.util.Random;

public class P5_Katta_Khan_Acharya_Model {
	
	static final char EMPTY = 'o';
	static final char TREE = 't';
	static final char STREET = 's';
	static final char CAR1 = '1';
	static final char CAR2 = '2';
	static final char CHICKEN = 'p';
	
	int score = 0;
	int counter = 0;
	char[][] map = new char[10][10];
	char[] startRow = {CHICKEN, EMPTY, TREE, TREE, TREE, TREE, TREE, TREE, TREE, TREE};
	char[] row1 = {EMPTY, EMPTY, TREE, TREE, TREE, TREE, TREE, TREE, TREE, TREE};
	char[] row2 = {TREE, TREE, EMPTY, EMPTY, TREE, TREE, TREE, TREE, TREE, TREE};
	char[] row3 = {TREE, TREE, TREE, TREE, EMPTY, EMPTY, TREE, TREE, TREE, TREE};
	char[] row4 = {TREE, TREE, TREE, TREE, TREE, TREE, EMPTY, EMPTY, TREE, TREE};
	char[] row5 = {TREE, TREE, TREE, TREE, TREE, TREE, TREE, TREE, EMPTY, EMPTY};
	char[] streetRow = {STREET, STREET, STREET, STREET, STREET, STREET, STREET, STREET, STREET, STREET};
	char previousSpace = EMPTY;
	
	public void initializeMap(){
		for(int i = 0; i < map[0].length; i++){
			map[map.length - 1][i] = startRow[i];
		}
		
		for(int i = map.length - 2; i >= 0; i--){
			Random x = new Random();
			int ran = x.nextInt(4) + 1;
			char[] row = row1;
			
			switch(ran){
				case 1:
					row = row1;
					break;
				case 2:
					row = row2;
					break;
				case 3:
					row = row3;
					break;
				case 4:
					row = row4;
					break;
				case 5:
					row = row5;
					break;
			}
			if(i % 2 == 0){
				row = streetRow;
			}
			for(int a = 0; a < map[0].length; a++){
				map[i][a] = row[a];
			}
			if(row == streetRow){
				Random y = new Random();
				boolean random = y.nextBoolean();
				
				if(random == true){
					map[i][0] = CAR1;
				} else {
					map[i][map[i].length - 1] = CAR2;
				}
			}
		}
	}


	public void initializeTopLine(){
		for(int i = map.length - 2; i >= 0; i--){
			for(int j = 0; j < map.length; j++){
				map[i + 1][j] = map[i][j];
			}
		}
		
		Random x = new Random();
		int ran = x.nextInt(4) + 1;
		char[] row = row1;
			
			switch(ran){
				case 1:
					row = row1;
					break;
				case 2:
					row = row2;
					break;
				case 3:
					row = row3;
					break;
				case 4:
					row = row4;
					break;
				case 5:
					row = row5;
					break;
			}
			counter++;
			if(counter % 2 == 0){
				row = streetRow;
			}
			//counter++;
			for(int a = 0; a < map[0].length; a++){
				map[0][a] = row[a];
			}
			if(row == streetRow){
				Random y = new Random();
				boolean random = y.nextBoolean();
				
				if(random == true){
					map[0][0] = CAR1;
				} else {
					map[0][map[0].length - 1] = CAR2;
				}
			}
	}
	public void act(){
		for(int i = 0; i < map.length; i++){
			int x = getCarX(i);
			if(x != -1){
				if(map[i][x] == CAR1){
					if(x == 9){
						map[i][x] = STREET;
						map[i][0] = CAR1;
					} else {
						map[i][x] = STREET;
						map[i][x + 1] = CAR1;
					}
				} else {
					if(x == 0){
						map[i][x] = STREET;
						map[i][map[x].length - 1] = CAR2;
					} else {
						map[i][x] = STREET;
						map[i][x - 1] = CAR2;
					}
				}
			}
		}
	}
	
	public int getCarX(int y){
		for(int i = 0; i < map[y].length; i++){
			if(map[y][i] == CAR1 || map[y][i] == CAR2){
				return i;
			}
		}
		return -1;
	}

	
	public void printMap(){
		for(int i = 0; i < map.length; i++){
			System.out.println();
			for(int a = 0; a < map[0].length; a++){
				System.out.print(map[i][a] + " ");
			}
		}
	}
	
	public int getChickenX(){
		int x = 0; 
		for(int i = 0; i < map.length; i++){
			for(int a = 0; a < map[i].length; a++){
				if(map[i][a] == CHICKEN){
					x = a;
				}
			}
		}
		return x;
	}
	
	public int getChickenY(){
		int y = 0; 
		for(int i = 0; i < map.length; i++){
			for(int a = 0; a < map[i].length; a++){
				if(map[i][a] == CHICKEN){
					y = i;
				}
			}
		}
		return y;
	}
	
	public void moveUp(){
		int x = getChickenX();
		int y = getChickenY();
		
		if(y - 1 >= 0 && map[y - 1][x]!= TREE){
			map[y][x] = previousSpace;
			previousSpace = map[y - 1][x];
			if(map[y - 1][x] == CAR1 || map[y - 1][x] == CAR2){
				
			}else{
				map[y - 1][x] = CHICKEN;
			}
			//map[y - 1][x] = CHICKEN;
		}
	}
	
	public void moveDown(){
		int x = getChickenX();
		int y = getChickenY();
		
		if(y + 1 < map.length && map[y + 1][x] != TREE){
			map[y][x] = previousSpace;
			previousSpace = map[y + 1][x];
			if(map[y + 1][x] == CAR1 || map[y + 1][x] == CAR2){
				
			}else{
				map[y + 1][x] = CHICKEN;
			}
			//map[y + 1][x] = CHICKEN;
		}
	}
	
	public void moveLeft(){
		int x = getChickenX();
		int y = getChickenY();
		
		if(x - 1 >= 0 && map[y][x - 1] != TREE){
			map[y][x] = previousSpace;
			previousSpace = map[y][x - 1];
			if(map[y][x - 1] == CAR1 || map[y][x - 1] == CAR2){
				
			}else{
				map[y][x - 1] = CHICKEN;
			}
			//map[y][x - 1] = CHICKEN;
		}
	}
	
	public void moveRight(){
		int x = getChickenX();
		int y = getChickenY();
		
		if(x + 1 < map.length && map[y][x + 1] != TREE){
			map[y][x] = previousSpace;
			previousSpace = map[y][x + 1];
			if(map[y][x + 1] == CAR1 || map[y][x + 1] == CAR2){
				
			}else{
				map[y][x + 1] = CHICKEN;
			}
		}
	}

	public boolean checkCollision(char[] row){
		boolean x = true;
		for(int i = 0; i < row.length; i++){
			if(row[i] == CHICKEN){
				x = false;			
			}
		}
		return x;
	}
	public boolean isDead(){
		boolean ret = true;
		for(int i = 0; i < map.length; i++){
			for(int j = 0; j < map[i].length; j++){
				if(map[i][j] == CHICKEN){
					ret = false;
				}
			}
		}
		return ret;
	}

}
